
#Unchange
A joint project between oskar and roulangxing(lydia).
The development name of the tool is "Unchange" and intends to 
reduce bug prone code by running code analysis focused around
immutability.

## License
See license file.

## Build and Run

When cloning this project it should be done recursively in order to also download the extendj repository which is a required
dependency. 

    git clone --recursive git@bitbucket.org:edan70/2018-bug-finding-oskar-ruolangxing.git
    
Or if you already cloned the project you can run:

    git submodule init
    git submodule update

This project is built with [Gradle][1], but you do not need to install Gradle to use it.
The following commands asserts that your working directory is in the folder Unchange which is located at the root of the project.
If you have Java installed, run the following commands to build the project:

    ./gradlew jar
    ./gradlew.bat jar (for windows)

Run the generated analyzer with

    java -jar Unchange.jar <Source Files>

If you want to start with a basic example try running

    java -jar ./Unchange.jar ./test/testfiles/warnings/parameter/UnfitParam.java 

The checker will then warn about an unfit parameter.

To run the tests, run:

    ./gradlew test
    
The repository contains a util package which is code borrowed from Oskar's external projects. This package contains 
plenty of junk which can be ignored.

[1]:https://gradle.org/