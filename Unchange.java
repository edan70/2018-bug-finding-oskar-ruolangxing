import org.extendj.ast.Frontend;
import org.extendj.ast.Program;
import utility.collections.immutable.Vector;

public class Unchange extends Frontend {
   private Unchange() {
   }

   private Unchange(String name, String version) {
      super(name, version);
   }

   public static Unchange create(String[] args) {
      Unchange unchange = new Unchange();
      unchange.run(args);
      return unchange;
   }

   public static void main(String[] args) {
      (new Unchange()).run(new String[]{"Unchange\\src\\sandbox\\ImmClass.java"});
   }

   public Vector getMutableClasses() {
      return Vector.fromList(this.program.mutableClasses());
   }

   public int run(String[] args) {
      int result = super.run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
      return result;
   }
}
