import org.extendj.ast.*;
import utility.collections.immutable.Vector;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class Unchange extends Frontend {
    
    private Unchange() {
    }
    
    private Unchange(final String name, final String version) {
        super(name, version);
    }
    
    public static Unchange create(String[] args){
        Unchange unchange = new Unchange();
        unchange.run(args);
        return unchange;
    }
    
    public static void main(String[] args) {
        Vector<String> strings = new Vector<>(args);
        boolean list = false;
        if(args[0].equals("-l"))
        {
            list = true;
            strings = strings.remove(args[0]);
        }
        String analyzeFile = strings.get(0);
        strings = strings.remove(analyzeFile);
    
    
    
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        PrintStream defaultErr = System.err;
        System.setErr(new PrintStream(errContent));
        
        
        Unchange unchange = new Unchange();
        int run = unchange.run(strings.add(analyzeFile).toList().toArray(new String[0]));
        if(list) {
            if(unchange.getMutableClasses().size() > 0)
                System.out.println("Mutable classes found:");
            unchange.getMutableClasses().foreach(mc -> System.out.println(mc.name()));
        }
    
        System.setErr(defaultErr);
//        for (final String s : new Vector<>(errContent.toString().split(System.lineSeparator())).filter(l -> l.contains(analyzeFile))) {
//            System.err.println(s);
//        }
    
        for (final String s : new Vector<>(errContent.toString().split(System.lineSeparator()))) {
            System.err.println(s);
        }
    }
    
    public Vector<ClassDecl> getMutableClasses(){
        return Vector.fromList(program.mutableClasses());
    }
    
    public int run(final String[] args) {
        int result = super.run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
        
        
        return result;
    }
}
