package utility;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Optional<T> {
    
    final public boolean isEmpty;
    private T item;
    
    private Optional() {
        isEmpty = true;
    }
    
    public Optional(T item) {
        this.item = item;
        isEmpty = false;
    }
    
    public T get() {
        if (isEmpty)
            throw new IllegalArgumentException("Can not get empty optional");
        return item;
    }
    
    public boolean isDefined() {
        return !isEmpty;
    }
    
    private static final Optional empt = new Optional();
    
    public static <T> Optional<T> empty() {
        return empt;
    }
    
    public <TNew> Optional<TNew> flatMap(Function<T, TNew> function) {
        if (isDefined())
            return new Optional<>(function.apply(item));
        return Optional.empty();
    }
    
    public void apply(Consumer<T> consumer) {
        if (isDefined())
            consumer.accept(item);
    }
    
    public Optional<T> fail(Predicate<T> test) {
        if (isDefined())
            if (test.test(item))
                return this;
            else
                return Optional.empty();
        return this;
    }
    
    public T getOrElse(Supplier<T> supplier) {
        if (isEmpty)
            return supplier.get();
        else
            return item;
    }
    
    public T getOrElse(final T v) {
        if (isDefined())
            return item;
        return v;
    }
}
