package utility;

import utility.collections.immutable.Val;

import java.util.Objects;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Tuple<TOne,TTWO> extends Val<Tuple<TOne,TTWO>>{

    private final TOne one;
    private final TTWO two;

    public Tuple(TOne one, TTWO two){
        this.one = one;
        this.two = two;
    }

    public TOne one() {
        return one;
    }

    public TTWO two() {
        return two;
    }

    @Override
    public String toString() {
        return "(" + one + "," + two + ")";
    }

    public <TNewOne> Tuple<TNewOne,TTWO> apply1(Function<TOne,TNewOne> function){
        return new Tuple<>(function.apply(one),two);
    }

    public <TNewTwo> Tuple<TOne,TNewTwo> apply2(Function<TTWO,TNewTwo> function){
        return new Tuple<>(one,function.apply(two));
    }
    
    
    public <TNewOne> Tuple<TNewOne,TTWO> use2(BiFunction<TOne,TTWO,TNewOne> function){
        return new Tuple<>(function.apply(one,two),two);
    }
    
    public <TNewTwo> Tuple<TOne,TNewTwo> use1(BiFunction<TOne,TTWO,TNewTwo> function){
        return new Tuple<>(one,function.apply(one,two));
    }
    
    public <TNewTwo> Tuple<TOne,TNewTwo> setTwo(final TNewTwo two) {
        return new Tuple<>(one,two);
    }
    
    @Override
    public Tuple<TOne, TTWO> get() {
        return this;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return Objects.equals(one, tuple.one) &&
                Objects.equals(two, tuple.two);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(one, two);
    }
}


