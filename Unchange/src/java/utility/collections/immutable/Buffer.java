package utility.collections.immutable;


import utility.Optional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@SuppressWarnings("unused")
public class Buffer<TElem> extends Seq<TElem> {
    
    private TElem[] elements;
    private int size;
    
    private Buffer(TElem[] elements) {
        this.elements = elements;
        size = elements.length;
    }
    
    public Buffer(int initialSize) {
        elements = (TElem[]) new Object[initialSize];
        size = 0;
    }
    
    public Buffer() {
        elements = (TElem[]) new Object[1];
        size = 0;
    }
    
    public Buffer(TElem element) {
        elements = (TElem[]) new Object[]{element};
        size = 1;
    }
    
    public Buffer(TElem elem1, TElem elem2) {
        elements = (TElem[]) new Object[]{elem1, elem2};
        size = 2;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3};
        size = 3;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4};
        size = 4;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4, elem5};
        size = 5;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4, elem5, elem6};
        size = 6;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4, elem5, elem6, elem7};
        size = 7;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7, TElem elem8) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4, elem5, elem6, elem7, elem8};
        size = 8;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7, TElem elem8, TElem elem9) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4, elem5, elem6, elem7, elem8, elem9};
        size = 9;
    }
    
    public Buffer(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7, TElem elem8, TElem elem9, TElem elem10) {
        elements = (TElem[]) new Object[]{elem1, elem2, elem3, elem4, elem5, elem6, elem7, elem8, elem9, elem10};
        size = 10;
    }
    
    public void append(TElem elem) {
        if (size == elements.length)
            doubleArray();
        elements[size] = elem;
        size += 1;
    }
    
    private void doubleArray() {
        TElem[] oldElements = elements;
        elements = (TElem[]) new Object[oldElements.length * 2];
        for (int i = 0; i < oldElements.length; i++) {
            elements[i] = oldElements[i];
        }
    }
    
    public int size() {
        return size;
    }
    
    @Override
    public List<TElem> toList() {
        ArrayList<TElem> list = new ArrayList<>();
        foreach(list::add);
        return list;
    }
    
    public boolean isEmpty() {
        return size() == 0;
    }
    
    public TElem get(int i) {
        if (i >= size)
            throw new IndexOutOfBoundsException();
        return elements[i];
    }
    
    public TElem removeAndReturn(int index){
        TElem out = elements[index];
        for (int i = index + 1; i < size(); i++) {
            elements[i - 1] = elements[i];
        }
        if (size() > 1)
            elements[size() - 1] = null;
        size--;
        return out;
    }
    
    public void remove(int index) {
        remove(elements[index]);
    }
    
    public void remove(TElem elem){
        int index = indexOf(elem);
        for (int i = index + 1; i < size(); i++) {
            elements[i - 1] = elements[i];
        }
        if (size() > 1)
            elements[size() - 1] = null;
        size--;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (TElem e : elements) {
            sb.append(e).append(",");
        }
        
        return "(" + sb.toString() + ")";
    }
    
    public Vector<TElem> toVector() {
        if (size == elements.length)
            return new Vector<>(elements);
        
        
        TElem[] elems = (TElem[]) new Object[size];
        
        for (int i = 0; i < elems.length; i++)
            elems[i] = elements[i];
        
        return new Vector<>(elems);
    }
    
    public int count(Predicate<TElem> predicate) {
        int sum = 0;
        for (int i = 0; i < size; i++) {
            if (predicate.test(elements[i]))
                sum++;
        }
        return sum;
    }
    
    public void foreach(Consumer<TElem> function) {
        for (int i = 0; i < size; i++) {
            function.accept(elements[i]);
        }
    }
    
    public void update(int index, TElem elem) {
        elements[index] = elem;
    }
    
    public <TNewElem> Buffer<TNewElem> map(Function<TElem, TNewElem> function) {
        Buffer<TNewElem> newElemBuffer = new Buffer<>(size);
        for (int i = 0; i < size; i++) {
            newElemBuffer.append(function.apply(elements[i]));
        }
        return newElemBuffer;
    }
    
    public Buffer<TElem> sort(Comparator<TElem> comparator) {
        ArrayList<TElem> list = toArrayList();
        Buffer<TElem> buffer = new Buffer<>(size);
        list.sort(comparator);
        list.forEach(buffer::append);
        return buffer;
    }
    
    public ArrayList<TElem> toArrayList() {
        ArrayList<TElem> list = new ArrayList<>();
        foreach(list::add);
        return list;
    }
    
    public TElem find(Predicate<TElem> predicate) {
        for (TElem e : elements) {
            if (predicate.test(e))
                return e;
        }
        throw new IllegalArgumentException("Could not satisfy predicate in buffer");
    }
    
    public Optional<TElem> tryFind(Predicate<TElem> predicate) {
        for (TElem e : elements) {
            if (predicate.test(e))
                return new Optional<>(e);
        }
        return Optional.empty();
    }
    
    public int indexOf(TElem e) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].equals(e))
                return i;
        }
        throw new IllegalStateException("Can not find index of element: " + e);
    }
    
    public int indexWhere(Predicate<TElem> predicate) {
        for (int i = 0; i < elements.length; i++) {
            if (predicate.test(elements[i]))
                return i;
        }
        throw new IllegalStateException("Can not find index where predicate is fulfilled");
    }
    
    public void replace(TElem oldElem, TElem newElem) {
        int index = indexOf(oldElem);
        elements[index] = newElem;
    }
    
    public static <TNewElem> Buffer<TNewElem> fill(int length, Supplier<TNewElem> supplier) {
        TNewElem[] elems = (TNewElem[]) new Object[length];
        for (int i = 0; i < elems.length; i++) {
            elems[i] = supplier.get();
        }
        return new Buffer<>(elems);
    }
    
    
    public void replaceAt(int index, TElem elem) {
        if (index >= size)
            throw new IndexOutOfBoundsException("Can not replace element outside vector");
        elements[index] = elem;
    }
    
    public boolean contains(Object elem) {
        for (final TElem element : elements) {
            if (element.equals(elem))
                return true;
        }
        return false;
    }
    
    public static <TType> Buffer<TType> fromArray(final TType[] data) {
        Buffer<TType> buf = new Buffer<>(data);
        buf.doubleArray();
        return buf;
    }
    
    public static Buffer<Byte> fromByteArray(final byte[] data) {
        Buffer<Byte> byteBuffer = new Buffer<>();
        for (final byte b : data) {
            byteBuffer.append(b);
        }
        return byteBuffer;
    
    }
    
    public TElem[] toArray() {
        return elements.clone();
    }
    
}
