package utility.collections.immutable;

public interface Linkable<TLinkable> {
    
    Vector<TLinkable> connections();
    
}
