package utility.collections.immutable;

public class Lock<TType> {

    public TType value;

    public Lock(TType value){
        this.value = value;
    }

}
