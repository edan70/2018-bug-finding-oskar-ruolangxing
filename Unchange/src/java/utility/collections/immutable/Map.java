package utility.collections.immutable;

import utility.Optional;

public interface Map<TKey,TValue> {

    TValue get(TKey key);
    Optional<TValue> tryGet(TKey key);
    boolean containsKey(TKey key);
    Map<TKey,TValue> put(TKey key, TValue value);
    
    Vector<TKey> keys();
    Vector<TValue> values();
    
    TKey getKey(TValue value);
    
    Optional<TKey> tryGetKey(TValue value);
    
    Map<TKey,TValue> removeValue(TKey innerData);
}
