package utility.collections.immutable;

import java.util.List;

public abstract class Seq<TElem> {
    public abstract TElem get(int index);
    public abstract int size();
    
    public abstract List<TElem> toList();
}
