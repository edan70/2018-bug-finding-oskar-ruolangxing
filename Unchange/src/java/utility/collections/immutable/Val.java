package utility.collections.immutable;

import java.util.function.Function;

public abstract class Val<TType> {
    
    public abstract TType get();
    
    
    public <TNew> TNew apply(Function<TType,TNew> function) {
        return function.apply(get());
    }
}
