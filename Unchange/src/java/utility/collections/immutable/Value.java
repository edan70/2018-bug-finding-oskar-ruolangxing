package utility.collections.immutable;

public class Value<TType>  extends Val<TType> {
    
    
    private final TType value;
    
    public Value(TType value) {
        this.value = value;
    }
    
    public TType get(){
        return value;
    }
}
