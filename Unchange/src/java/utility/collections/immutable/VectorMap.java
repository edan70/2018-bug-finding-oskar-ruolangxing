package utility.collections.immutable;

import utility.Optional;
import utility.Tuple;


public class VectorMap<TKey,TValue> extends Vector<Tuple<TKey,TValue>> implements Map<TKey,TValue> {
    
    public VectorMap(final Tuple<TKey, TValue>[] elements) {
        super(elements);
    }
    
    public VectorMap() {
    }
    
    public VectorMap(Vector<Tuple<TKey,TValue>> elements) {
        super(elements.elements);
    }
    
    @Override
    public TValue get(final TKey key) {
        Optional<Tuple<TKey, TValue>> tupleOptional = this.tryFind(e -> e.one().equals(key));
        if(tupleOptional.isDefined())
            return tupleOptional.get().two();
        throw new  IllegalStateException("Can not get key not in map: " + key);
    }
    
    @Override
    public Optional<TValue> tryGet(final TKey key) {
        return tryFind(k -> k.one().equals(key)).flatMap(Tuple::two);
    }
    
    @Override
    public boolean containsKey(final TKey key) {
        return this.tryFind(e -> e.one().equals(key)).isDefined();
    }
    
    @Override
    public Map<TKey, TValue> put(final TKey key, final TValue value) {
        return new VectorMap<>(add(new Tuple<>(key,value)));
    }
    
    @Override
    public Vector<TKey> keys() {
        return map(Tuple::one);
    }
    
    @Override
    public Vector<TValue> values() {
        return map(Tuple::two);
    }
    
    @Override
    public TKey getKey(final TValue value) {
        return this.tryFind(t -> t.two().equals(value)).get().one();
    }
    
    @Override
    public Optional<TKey> tryGetKey(final TValue value) {
        return tryFind(t -> t.two().equals(value)).flatMap(Tuple::one);
    }
    
    @Override
    public Map<TKey,TValue> removeValue(final TKey key) {
        return new VectorMap<>(filter(t -> t.one().equals(key)));
    }
}
