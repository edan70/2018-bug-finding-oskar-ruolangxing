package utility.collections.immutable.type;

public class L<TType,TList extends L> {

    public final TType value;
    public final TList link;
    
    public L(final TType value, final TList link) {
        this.value = value;
        this.link = link;
    }
    
    public Object get(int i) {
        if(i == 0)
            return value;
        return link.get(i - 1);
    }
    
    
    
}
