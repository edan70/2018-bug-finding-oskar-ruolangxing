package utility.collections.mutable;

import utility.Optional;
import utility.Tuple;
import utility.collections.immutable.Buffer;
import utility.collections.immutable.Map;
import utility.collections.immutable.Vector;

public class BufferMap<TKey,TValue> extends Buffer<Tuple<TKey,TValue>> implements MutableMap<TKey,TValue>{
    
    @Override
    public TValue get(final TKey key) {
        
        Optional<Tuple<TKey, TValue>> tupleOptional = this.tryFind(e -> e.equals(key));
        if(tupleOptional.isDefined())
            return tupleOptional.get().two();
        throw new  IllegalStateException("Can not get key not in map: " + key);
    }
    
    @Override
    public Optional<TValue> tryGet(final TKey key) {
        return tryFind(k -> k.equals(key)).flatMap(Tuple::two);
    }
    
    @Override
    public boolean containsKey(final TKey key) {
        return this.tryFind(e -> e.one().equals(key)).isDefined();
    }
    
    @Override
    public Map<TKey, TValue> put(final TKey key, final TValue value) {
        return null;
    }
    
    @Override
    public Vector<TKey> keys() {
        return map(Tuple::one).toVector();
    }
    
    @Override
    public Vector<TValue> values() {
        return map(Tuple::two).toVector();
    }
    
    @Override
    public TKey getKey(final TValue value) {
        return this.tryFind(t -> t.two().equals(value)).get().one();
    }
    
    @Override
    public Optional<TKey> tryGetKey(final TValue value) {
        return tryFind(t -> t.two().equals(value)).flatMap(Tuple::one);
    }
    
    @Override
    public Map<TKey, TValue> removeValue(final TKey innerData) {
        return null;
    }
    
    
    @Override
    public void set(final TKey k, final TValue v) {
        if(containsKey(k))
            this.replaceAt(indexWhere(t -> t.one().equals(k)),new Tuple<>(k,v));
        else
            append(new Tuple<>(k,v));
        
    }
}
