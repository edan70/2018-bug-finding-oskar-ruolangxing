package utility.collections.mutable;

import utility.collections.immutable.Map;

public interface MutableMap<TKey,TValue> extends Map<TKey,TValue> {

    void set(TKey k, TValue v);


}
