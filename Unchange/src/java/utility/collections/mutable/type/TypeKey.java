package utility.collections.mutable.type;

import java.util.Objects;

public class TypeKey<TType> {
    public final long id;
    
    public TypeKey(final long id) {
        this.id = id;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final TypeKey<?> typeKey = (TypeKey<?>) o;
        return id == typeKey.id;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
    
    @Override
    public String toString() {
        return "TypeKey{" +
                "id=" + id +
                '}';
    }
}
