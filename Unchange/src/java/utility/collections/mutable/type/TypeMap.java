package utility.collections.mutable.type;

import utility.collections.immutable.Map;
import utility.collections.immutable.Vector;
import utility.collections.immutable.VectorMap;

public class TypeMap<TBaseType> {
    
    private long id = 0;
    private Map<TypeKey<? extends TBaseType>,TBaseType> map = new VectorMap<>();
    
    public synchronized <TType extends TBaseType> TypeKey<TType> add(TType elem){
        TypeKey<TType> key = new TypeKey<>(id);
        id += 1;
        map = map.put(key,elem);
        return key;
    }
    
    public synchronized <TType extends TBaseType> TType get(TypeKey<TType> key){
        TBaseType base = map.get(key);
        return (TType)base;
    }
    
    
    public <TType extends TBaseType> TypeKey<TType> getKey(final TType cameraModule) {
        return (TypeKey<TType>) map.getKey(cameraModule);
    }
    
    
    public Vector<TypeKey<? extends TBaseType>> keys() {
        return map.keys();
    }
    
    public Vector<TBaseType> values() {
        return map.values();
    }
    
    public <TType extends TBaseType> void remove(final TypeKey<TType> innerData) {
        map.removeValue(innerData);
    }
}
