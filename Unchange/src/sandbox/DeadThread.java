

public class DeadThread {
    
    static Thread fieldTh;
    
    public static void main(String[] args) throws InterruptedException {
        Thread th = new Thread(() -> System.out.println("hello"));
        th.start();
        new Thread();
        System.out.println(new Thread());
        new Thread().start();
        fieldTh = new Thread();
        th.join();
        
    }
    
    
    public Thread getThread() {
        return new Thread();
    }
}