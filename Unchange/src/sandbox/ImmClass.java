
class SuperClass {
    final int aisjd = 4;
}

class MutablePoint {
    int x = 4;
    int y = 4;
}

public class ImmClass extends SuperClass{

    
    final int t = 4;
    final int g = 4;
    //static int iAmStatic = 30;
    
    public void foo(int a, SuperClass s){
        System.out.println("wow");
    }
    
    public void bar(MutablePoint mp){
        System.out.println("wow");
        mp.x = 3;
    }
    
}
