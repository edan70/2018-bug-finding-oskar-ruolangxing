
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.*;

@SuppressWarnings("unused")
public class Vector<TElem> implements Iterable<TElem>{

    protected final TElem[] elements;

    private Vector(TElem[] elements, boolean unused){
        this.elements = elements;
    }

    public Vector(TElem[] elements){
        this.elements = (TElem[])new Object[elements.length];
        for(int i = 0; i < elements.length; i++)
            this.elements[i] = elements[i];
    }

    private Vector(TElem[] elements, TElem elem){
        this.elements = (TElem[])new Object[elements.length + 1];
        for(int i = 0; i < elements.length; i++)
            this.elements[i] = elements[i];
        this.elements[elements.length] = elem;
    }

    private Vector(TElem[] elements1, TElem[] elements2){
        this.elements = (TElem[])new Object[elements1.length + elements2.length];
        for(int i = 0; i < elements1.length; i++)
            this.elements[i] = elements1[i];
        for(int i = 0; i < elements2.length; i++)
            elements[elements1.length + i] = elements2[i];
    }

    public Vector() {
        elements = (TElem[])new Object[0];
    }

    public Vector(TElem element){
        elements = (TElem[])new Object[]{element};
    }

    public Vector(TElem elem1, TElem elem2){
        elements = (TElem[])new Object[]{elem1,elem2};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3){
        elements = (TElem[])new Object[]{elem1,elem2,elem3};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4,elem5};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4,elem5,elem6};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4,elem5,elem6,elem7};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7, TElem elem8){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4,elem5,elem6,elem7,elem8};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7, TElem elem8, TElem elem9){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4,elem5,elem6,elem7,elem8,elem9};
    }

    public Vector(TElem elem1, TElem elem2, TElem elem3, TElem elem4, TElem elem5, TElem elem6, TElem elem7, TElem elem8, TElem elem9, TElem elem10){
        elements = (TElem[])new Object[]{elem1,elem2,elem3,elem4,elem5,elem6,elem7,elem8,elem9,elem10};
    }
    
    public static <TElem> Vector<TElem> fromList(final List<TElem> list) {
        return new Vector<>(list.toArray((TElem[]) new Object[0]), false);
    }
    
    public int size() {
        return elements.length;
    }
    
    public List<TElem> toList() {
        ArrayList<TElem> list = new ArrayList<>();
        foreach(list::add);
        return list;
    }
    
    public <TRes> TRes foldLeft(TRes initial, BiFunction<TRes,TElem,TRes> function){
        TRes foldStock = initial;
        for (TElem elem : elements) {
            foldStock = function.apply(foldStock, elem);
        }
        return foldStock;
    }
    
    
    
    
    private TElem last() {
        return elements[elements.length - 1];
    }
    
    private TElem first() {
        return elements[0];
    }
    
    public TElem get(int i) {
        if(i >= size())
            throw new IndexOutOfBoundsException();
        return elements[i];
    }

    public Vector<TElem> add(TElem elem){
        return new Vector<>(elements,elem);
    }

    public Vector<TElem> addAll(Vector<TElem> vec){
        return new Vector<TElem>(elements,vec.elements);
    }

    public Iterator<TElem> iterator() {
        return new VectorIterator();
    }

//    public Optional<TElem> tryFind(Predicate<TElem> predicate) {
//
//        for(TElem e : this){
//            if(predicate.test(e))
//                return new Optional<>(e);
//        }
//        return Optional.empty();
//    }
    
//    public Vector<TElem> remove(final TElem node) {
//        return removeAt(indexOf(node));
//    }
    
    
    
    public boolean contains(final TElem node) {
        for (final TElem element : elements) {
            if(node.equals(element))
                return true;
        }
        return false;
    }
    
    public Vector<TElem> replace(TElem oldElem, TElem newElem) {
        Vector<TElem> newVec = new Vector<>(elements);
        newVec.elements[newVec.indexOf(oldElem)] = newElem;
        return newVec;
    }
    
    public int indexWhere(Predicate<TElem> test) {
        for (int i = 0; i < elements.length; i++) {
            if(test.test(elements[i]))
                return i;
        }
        throw new IllegalArgumentException("Could not find positive in vector");
    }
    
    public class VectorIterator implements Iterator<TElem> {

        int i = -1;

        @Override
        public TElem next() {
            i += 1;
            return elements[i];
        }

        @Override
        public boolean hasNext() {
            return i < size() - 1;
        }
    }

    public int indexOf(Object elem){
        for(int i = 0; i < size(); i++)
            if(elements[i].equals(elem))
                return i;
        throw new IllegalArgumentException("Element not present in this vector");
    }

    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(TElem e : elements){
            sb.append(e).append(",");
        }

        return "(" + sb.toString() + ")";
    }

    public String toConcatenatedString(){
        StringBuilder sb = new StringBuilder();
        for(TElem e : elements){
            sb.append(e);
        }
        return sb.toString();
    }
    
    

    public void foreach(Consumer<TElem> function){
        for (TElem elem : elements){
            function.accept(elem);
        }

    }


    public static <TNewElem> Vector<TNewElem> fill(int length, Supplier<TNewElem> supplier){
        TNewElem[] elems = (TNewElem[]) new Object[length];
        for(int i = 0; i < elems.length; i++){
            elems[i] = supplier.get();
        }
        return new Vector<>(elems,false);
    }
}
