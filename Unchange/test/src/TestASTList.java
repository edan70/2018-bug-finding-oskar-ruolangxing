import org.extendj.JavaDumpTree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import utility.collections.immutable.Vector;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.function.IntFunction;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestASTList {
    
    private static final PrintStream defaultOut = System.out;
    private static final PrintStream defaultErr = System.err;
    
    
    @Test
    public void run(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));
        System.setOut(new PrintStream(outContent));
        File srcFile = new File(Util.TEST_DIRECTORY,"astlist");
        File[] files = srcFile.listFiles();
        String[] fileNames = Arrays.stream(files).map(f -> f.getPath()).toArray(value -> new String[value]);
        Unchange unchange = Unchange.create(fileNames);
        
        System.setOut(defaultOut);
        System.setErr(defaultErr);
        defaultOut.println(outContent.toString());
        defaultErr.println(errContent.toString());
        Vector<String> strings = new Vector<>(errContent.toString().split(System.lineSeparator()));
        Vector<String> listWarnings = strings.filter(s -> s.contains("List.java"));


//        JavaDumpTree dumpTree = new JavaDumpTree();
//        dumpTree.run(new String[]{srcFile.getPath()});
        unchange.getMutableClasses().foreach(c -> System.out.println(c.name()));
        assertEquals("could not find mutable class in file.(" + srcFile.getName() + ":30)", 4, listWarnings.size());
    }
    
}
