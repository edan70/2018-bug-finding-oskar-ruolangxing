import org.extendj.JavaDumpTree;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TestImmutableClasses {
    
    private static final PrintStream defaultOut = System.out;
    private static final PrintStream defaultErr = System.err;
    
    private final File srcFile;
    
    public TestImmutableClasses(File srcFile){
        this.srcFile = srcFile;
    }
    
    @Test
    public void run(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));
        System.setOut(new PrintStream(outContent));
        Unchange unchange = Unchange.create(new String[]{srcFile.getPath()});
        
        System.setOut(defaultOut);
        System.setErr(defaultErr);
        defaultOut.println(outContent.toString());
        defaultErr.println(errContent.toString());
    
    
        JavaDumpTree dumpTree = new JavaDumpTree();
        dumpTree.run(new String[]{srcFile.getPath()});
    
        assertEquals("found mutable classes in file.(" + srcFile.getName() + ":30)\n"+unchange.getMutableClasses().toConcatenatedString(),
                0,unchange.getMutableClasses().size());
    }
    
    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        LinkedList<Object[]> objects = new LinkedList<>();
        File file = new File(Util.TEST_DIRECTORY,"immutable");
        for (final File listFile : file.listFiles()) {
            if(listFile.getName().endsWith(".java"))
                objects.add(new Object[]{listFile});
        }
        return objects;
    }
}
