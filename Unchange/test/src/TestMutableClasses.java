import org.extendj.JavaDumpTree;
import org.extendj.JavaPrettyPrinter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.LinkedList;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TestMutableClasses {
    
    private static final PrintStream defaultOut = System.out;
    private static final PrintStream defaultErr = System.err;
    
    private final File srcFile;
    
    public TestMutableClasses(File srcFile){
        this.srcFile = srcFile;
    }
    
    @Test
    public void run(){
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errContent));
        System.setOut(new PrintStream(outContent));
        Unchange unchange = Unchange.create(new String[]{srcFile.getPath()});
        
        System.setOut(defaultOut);
        System.setErr(defaultErr);
        defaultOut.println(outContent.toString());
        defaultErr.println(errContent.toString());
    
    
        JavaDumpTree dumpTree = new JavaDumpTree();
        dumpTree.run(new String[]{srcFile.getPath()});
    
        assertTrue("could not find mutable class in file.(" + srcFile.getName() + ":30)",
                unchange.getMutableClasses().tryFind(n -> (n.name().replaceAll("<.*>","") + ".java").equals(srcFile.getName())).isDefined());
    }
    
    @Parameterized.Parameters(name = "{0}")
    public static Iterable<Object[]> getTests() {
        LinkedList<Object[]> objects = new LinkedList<>();
        File file = new File(Util.TEST_DIRECTORY,"mutable");
        for (final File listFile : file.listFiles()) {
            if(listFile.getName().endsWith(".java"))
                objects.add(new Object[]{listFile});
        }
        return objects;
    }
}
