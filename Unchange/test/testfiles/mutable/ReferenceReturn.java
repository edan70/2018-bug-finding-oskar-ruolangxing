
class MutableObject2 {
    public int a = 4;
    public int b = 4;
}


public class ReferenceReturn {
    private final MutableObject2 reference = new MutableObject2();
    
    public MutableObject2 referenceLeaker(){
        return reference;
    }
    
    
    
}

