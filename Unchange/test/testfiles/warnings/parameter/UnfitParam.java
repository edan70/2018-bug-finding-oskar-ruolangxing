
class MutableClass {
    
    private int a = 3;
    
    void bar(){
        a = 4;
    }
    
}


public class UnfitParam {
    
    
    public void foo(MutableClass unfitParam){
        unfitParam.bar();
    }
    
    
    
}